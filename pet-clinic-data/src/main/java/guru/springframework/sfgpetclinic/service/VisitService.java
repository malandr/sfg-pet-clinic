package guru.springframework.sfgpetclinic.service;

import guru.springframework.sfgpetclinic.model.Visit;
import guru.springframework.sfgpetclinic.service.CrudService;

public interface VisitService extends CrudService<Visit, Long> {


}
